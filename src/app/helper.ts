export class Helper {

  public static random(min = 0, max, exclusions?: number[]) {
    const num = Math.floor(Math.random() * (max - min)) + min
    if (exclusions) {
      return exclusions.some( e => e === num) ? this.random(min, max, exclusions) : num
    } else {
      return num
    }
  }
}
