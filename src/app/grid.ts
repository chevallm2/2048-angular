import { Helper } from './helper'
import { Cell } from './cell'

export class Grid {

  data = []

  private static isCellEmpty(cell): boolean {
    return cell === 0
  }

  constructor() {
    this.initGrid()
  }

  private initGrid() {
    // create a 2d grid
    for (let line = 0; line < 4; line++) {
      const l = []
      for (let column = 0; column < 4; column++) {
        l.push(new Cell(line, column))
      }
      this.data.push(l)
    }

    // put 2 numbers (2 or 4) on random empty cell
    for (let i = 0; i < 2; i++) {
      this.spawn()
    }

    console.log(this.data)
  }

  private getRandomCell( empty = false ) {
    let line = Helper.random(0, 4)
    let column = Helper.random(0, 4)
    let cell = this.getCell(line, column)

    if (empty) {
      if (cell.empty) {
        return cell
      } else {
        const fails: Cell[] = []
        while (!cell.empty) {
          fails.push(cell)
          line = Helper.random(0, 4, fails.map( f => f.line))
          column = Helper.random(0, 4, fails.map( f => f.column))
          cell = this.getCell(line, column)
        }
      }
    } else {
      return cell
    }

  }

  public getCell(line: number, column: number): Cell {
    if (this.data[line][column].valid) {
      return this.data[line][column]
    } else {
      return null
    }
  }

  public setCell(line: number, column: number, cell: Cell): void {
    this.data[line][column] = cell;
  }

  private spawn() {
    const randomEmptyCell = this.getRandomCell(true)
    const value = Math.random() < 0.7 ? 2 ** 1 : 2 ** 2
    this.data[randomEmptyCell.line][randomEmptyCell.column].value = value
  }

  public emptyCell(line: number, column: number): void {
    this.data[line][column].value = 0
  }




}
