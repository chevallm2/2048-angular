export class Cell {

  line: number
  column: number
  value: number

  get empty(): boolean {
    return this.value === 0
  }

  get valid() {
    return ((this.line >= 0 || this.line <= 4) || (this.column >= 0 || this.column <= 4))
  }

  constructor(line: number, column: number) {
    this.line = line
    this.column = column
    this.value = 0
  }
}
