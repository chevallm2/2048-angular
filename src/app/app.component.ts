import { Component, OnInit, HostListener } from '@angular/core';
import { Grid } from './grid';
import { Cell } from './cell';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {

  grid = new Grid()

  ngOnInit() {

  }

  @HostListener('document:keyup', ['$event'])
  public onKeyUp(event: KeyboardEvent): void {
    const upKeys = [ 'z', 'Z', 'ArrowUp' ]
    const downKeys = [ 's', 'S', 'ArrowDown' ]
    const leftKeys = [ 'q', 'Q', 'ArrowLeft' ]
    const rightKeys = [ 'd', 'D', 'ArrowRight' ]

    const goodKeys = [ ...upKeys, ...downKeys, ...leftKeys, ...rightKeys]
    if (goodKeys.includes(event.key)) {
      if (upKeys.includes(event.key)) {

      } else if (downKeys.includes(event.key)) {

      } else if (leftKeys.includes(event.key)) {

      } else if (rightKeys.includes(event.key)) {

      }
    } else {
      return
    }
  }

  private move(direction: 'up' | 'down' | 'left' | 'right'): void {
    for (let line = 0; line < 4; line++) {
      for (let column = 0; column < 4; column++) {
        const cell = this.grid.getCell(line, column)
        if (cell.value > 0) {
          // move
          const lineOffset = direction === 'left' || direction === 'right' ? (direction === 'left' ? -1 : 1) : 0
          const columnOffset = direction === 'up' || direction === 'down' ? (direction === 'up' ? -1 : 1) : 0
          const nextCell = this.grid.getCell(line + lineOffset, column + columnOffset)
          if (nextCell.valid) {
            if (nextCell.value === cell.value) {
              this.merge(cell, nextCell, direction)
            } else {

            }
          } else {
            continue
          }
        } else {
          continue
        }
      }
    }
  }

  private merge(cell: Cell, nextCell: Cell, direction: 'up' | 'down' | 'left' | 'right'): void {
    const cells = [cell, nextCell]
    let hostCell
    let mergedCell
    switch (direction) {
      case 'up':
        [hostCell, mergedCell] = cells.sort( (a: Cell, b: Cell) => b.column - a.column)
        break
      case 'down':
        [hostCell, mergedCell] = cells.sort( (a: Cell, b: Cell) => b.column + a.column)
        break
      case 'left':
        [hostCell, mergedCell] = cells.sort( (a: Cell, b: Cell) => b.line - a.line)
        break
      case 'right':
        [hostCell, mergedCell] = cells.sort( (a: Cell, b: Cell) => b.line + a.line)
        break
    }
    this.grid.emptyCell(mergedCell.line, mergedCell.column)
    this.grid.setCell(hostCell.line, hostCell.column, cell)
  }

}
